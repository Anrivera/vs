﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Services
{
    public class EmailServices : IEmailService
    {
        private readonly ILogger<EmailServices> _logger;
        public EmailServices(ILogger<EmailServices> logger)
        {
            _logger = logger;
        }
        public void EnviarCorreo()
        {
            _logger.LogWarning("Enviando Correo");
        }
    }

    public interface IEmailService
    {
        void EnviarCorreo();
    }
}
