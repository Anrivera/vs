﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.VisualStudio.Web.CodeGeneration;
using Serilog;
using Serilog.Sinks.MSSqlServer;
using WebApp.Models;
using WebApp.Services;

namespace WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("defaultConnection"))
            .EnableSensitiveDataLogging(true)
            .UseLoggerFactory(new LoggerFactory()));
            //services.AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase("paisDB"));-->Base de datos en memoria

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options => options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,///validar emisor
                    ValidateAudience = true,///validar audiencia
                    ValidateLifetime = true,///vida del token, si no ha expirado
                    ValidateIssuerSigningKey = true,///validar llave secreta a utilizar
                                                    ///emisor y audiencia valida (colocar en un archivo de configuración)
                    ValidIssuer = "registro.ufg.edu.sv",
                    ValidAudience = "registro.ufg.edu.sv",
                    /// configurando la llave secreta a utilizar
                    IssuerSigningKey = new SymmetricSecurityKey(
                        Encoding.UTF8.GetBytes(Configuration["Llave_super_secreta"])),
                    ClockSkew = TimeSpan.Zero ///no se hagan ajustes temporales en el algoritmo que determina si el token a expirado
                });

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(ConfigureJson);

            services.AddSingleton<IEmailService, EmailServices>();

            var columnOption = new ColumnOptions();
            columnOption.AdditionalDataColumns = new Collection<DataColumn>
            {
                new DataColumn {DataType = typeof (string), ColumnName = "OtherData"},
            };
            //columnOption.Store.Remove(StandardColumn.MessageTemplate);

            services.AddSingleton<Serilog.ILogger>(options => 
            {
                var connString = Configuration["Serilog:defaultConnection"];
                var tableName = Configuration["Serilog:TableName"];
                return new LoggerConfiguration().WriteTo.MSSqlServer(connString, tableName, restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Verbose, autoCreateSqlTable: true, columnOptions: columnOption).CreateLogger();
            });
        }

        private void ConfigureJson(MvcJsonOptions obj) {
            obj.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddFile("Log-{Date}.txt", LogLevel.Warning, null,true);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseAuthentication();
            app.UseMvc();
            /*
            if (!context.Paises.Any())
            {
                context.Paises.AddRange(new List<Pais>()
                {
                    new Pais(){Nombre = "Republica Dominicana", Provincias = new List<Provincia>(){
                        new Provincia() {Nombre = "Azua" }
                    } },
                    new Pais(){Nombre = "Mexico", Provincias = new List<Provincia>(){
                        new Provincia() {Nombre = "Puebla" },
                        new Provincia() {Nombre = "Queretaro" }
                    } },
                    new Pais(){Nombre = "Argentina" }
                });

                context.SaveChanges();
            }
            */
            
        }
    }
}
