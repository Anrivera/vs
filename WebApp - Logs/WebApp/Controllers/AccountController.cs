﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {

        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IConfiguration _configuration;
        private readonly IEmailSender _emailSender;

        public AccountController(SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager, IConfiguration configuration, IEmailSender emailSender)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _configuration = configuration;
            _emailSender = emailSender;
        }

        [Route("Create")]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] UserInfo model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email
                };
                var userr = new ApplicationUser { UserName = model.Email, Email = model.Email };

                var result = await _userManager.CreateAsync(user, model.Password);
                var x = await _userManager.FindByEmailAsync(model.Email);

                if (result.Succeeded)
                {
                    string token = await _userManager.GenerateEmailConfirmationTokenAsync(userr);
                    token = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(token));
                    var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = x.Id, token = token }, protocol: Request.Scheme);

                    await _emailSender.SendEmailAsync(model.Email, "Confirm your email",
                        $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");
                    //await _signInManager.SignInAsync(user, isPersistent: false);
                    return Ok("Usernam y password valid: " + result);
                }
                //return BuildToken(model);
                else
                    return BadRequest("Usernam or password invalid: " + result);
            }
            else
                return BadRequest(ModelState);
        }

        [Route("ConfirmEmail")]
        [HttpGet]
        public async Task<ActionResult> ConfirmEmail(string userId, string token)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(userId);

                token = Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(token));

                if (userId == null || token == null)
                {
                    return BadRequest("Error");
                }

                //token = HttpUtility.UrlDecode(token);
                IdentityResult result = await _userManager.ConfirmEmailAsync(user, token);
                var resultado = await _userManager.ConfirmEmailAsync(user, token);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
            return Ok("df");

        }


        [Route("Login")]
        [HttpPost]
        public async Task<IActionResult> Login([FromBody] UserInfo userInfo)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(userInfo.Email, userInfo.Password, isPersistent: true, lockoutOnFailure: false);

                if (result.Succeeded)
                {
                    return BuildToken(userInfo);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt");
                    return BadRequest(ModelState);
                }
            }
            else
                return BadRequest(ModelState);
        }
        //Claim - conjunto de información en las cuales se pueden confiar
        //Claim = es un nombre, identificador y un valor
        private IActionResult BuildToken(UserInfo userInfo)
        {
            var expiration = DateTime.UtcNow.AddHours(1);//expiración de token
            //Creación de claims
            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.UniqueName, userInfo.Email),//son conjunto de nombres que los claims puede tener, los cuales estan reconocidos por los estandares de JWT
                new Claim("miValor", "Lo que yo quiera"),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };
            //Llave
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Llave_super_secreta"]));//convertir un string a un arreglo de byte
            //Crear credenciales
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            //Construir token
            JwtSecurityToken token = new JwtSecurityToken(
                issuer: "registro.ufg.edu.sv",
                audience: "registro.ufg.edu.sv",
                claims: claims,
                expires: expiration,
                signingCredentials: creds);

            return Ok(new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token),
                expiration
            });
        }
    }
}