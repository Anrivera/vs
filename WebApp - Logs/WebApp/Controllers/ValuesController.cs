﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApp.Services;

namespace WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly ILogger<ValuesController> _logger;
        private readonly IEmailService _emailService;
        private readonly Serilog.ILogger _seriLogger;
        public ValuesController(ILogger<ValuesController> logger, IEmailService emailService, Serilog.ILogger seriLogger) {
            _logger = logger;
            _emailService = emailService;
            _seriLogger = seriLogger;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            var itemNumber = 10;
            var itemCount = 999;
            var user = new { Name = "Nick", Id = "nblumhardt" };

            _logger.LogDebug("Este es un mensaje debug");
            _logger.LogWarning("Este es un mensaje de warning");
            _logger.LogError("Este es un mensaje de error");


            _seriLogger.Information("Logged on user {@User}", user);
            _seriLogger.ForContext("OtherData", "reingreso").Information("Logged on user {@User}", user);
            _seriLogger.Error("Este es un mensaje de error");
            _seriLogger.Warning("Processing item {ItemNumber} of {ItemCount}", itemNumber, itemCount);
            _seriLogger.Debug("Este es un mensaje de debug");
            _seriLogger.ForContext("OtherData", "Test Data").Information("Index method called!!!");

            _emailService.EnviarCorreo();
            return new string[] { "serilog.Extensions.Logging.File", "Serilog.Sinks.MSSqlServer" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
