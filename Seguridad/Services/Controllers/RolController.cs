﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Models.Seguridad;

namespace Services.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolController : ControllerBase
    {
        private readonly RoleManager<ApplicationRol> _rolManager;
        public RolController(RoleManager<ApplicationRol> roleManager)
        {
            _rolManager = roleManager;
        }

        [Route("Create")]
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var rol1 = new ApplicationRol();
            rol1.Name = "ADMINISTRATIVO";
            IdentityResult x = await _rolManager.CreateAsync(rol1);

            var rol2 = new ApplicationRol();
            rol2.Name = "DOCENTE";
            await _rolManager.CreateAsync(rol2);

            var rol3 = new ApplicationRol();
            rol3.Name = "ESTUDIANTE";
            await _rolManager.CreateAsync(rol3);

            var rol4 = new ApplicationRol();
            rol4.Name = "INVITADO";
            await _rolManager.CreateAsync(rol4);

            var rol5 = new ApplicationRol();
            rol5.Name = "SUPERADMIN";
            await _rolManager.CreateAsync(rol5);

            return Ok("ok");
        }
    }
}