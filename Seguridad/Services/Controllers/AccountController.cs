﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Microsoft.AspNetCore.Identity.UI.Services;
using System.Text;
using Microsoft.AspNetCore.WebUtilities;
using System.Text.Encodings.Web;
using Models.Seguridad;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace Services.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IConfiguration _configuration;
        private readonly IEmailSender _emailSender;

        public AccountController(SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager, IConfiguration configuration, IEmailSender emailSender)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _configuration = configuration;
            _emailSender = emailSender;
        }

        [Route("Create")]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] ApplicationUser model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IdentityResult result = await _userManager.CreateAsync(model, model.PasswordHash);

                    if (!result.Succeeded)
                        throw new Exception("Usernam or password invalid: " + result);

                    ApplicationUser user = await _userManager.FindByEmailAsync(model.Email);

                    if (user is null)
                        throw new Exception("Error");

                    string token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    token = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(token));

                    // Generar url
                    var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, token }, protocol: Request.Scheme);
                    // Enviar correo
                    await _emailSender.SendEmailAsync(model.Email, "Confirme su correo electrónico", $"Porfavor confirme su cuenta por <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>Click aquí</a>.");

                    return Ok("Usernam y password valid: " + result);
                }

                return BadRequest(ModelState);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [Route("ConfirmEmail")]
        [HttpGet]
        public async Task<ActionResult> ConfirmEmail(string userId, string token)
        {
            try
            {
                ApplicationUser user = await _userManager.FindByIdAsync(userId);

                token = Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(token));

                if (userId == null || token == null)
                    throw new Exception("Error");

                IdentityResult result = await _userManager.ConfirmEmailAsync(user, token);

                if (result.Succeeded)
                    return Ok("df");
                else
                    throw new Exception("No se pudo confirmar el correo");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpGet("Edit/{id}")]
        public async Task<ActionResult> Edit(string Id)
        {
            try
            {
                ApplicationUser user = await _userManager.FindByIdAsync(Id);

                if (user != null)
                {
                    if (!await _userManager.IsEmailConfirmedAsync(user))
                        throw new Exception("You must have a confirmed email to log in.");
                }

                return Ok(user);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpPost("Update/{id}")]
        public async Task<ActionResult> Update(string Id, [FromBody] ApplicationUser model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ApplicationUser user = await _userManager.FindByIdAsync(Id);

                    if (user is null)
                        throw new Exception("Error");

                    IdentityResult removerPassword = await _userManager.RemovePasswordAsync(user);

                    if (!removerPassword.Succeeded)
                        throw new Exception("ERROR");

                    user.UserName = model.Email;
                    user.Email = model.Email;
                    user.PhoneNumber = model.PhoneNumber;

                    IdentityResult validarPassword = await _userManager.AddPasswordAsync(user, model.PasswordHash);

                    if (!validarPassword.Succeeded)
                        throw new Exception("ERROR");
                    
                    IdentityResult result = await _userManager.UpdateAsync(user);

                    if (result.Succeeded)
                        return Ok("Usernam y password valid: " + result);

                    throw new Exception("ERROR");
                }

                return BadRequest(ModelState);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("Login")]
        [HttpPost]
        public async Task<IActionResult> Login([FromBody] ApplicationUser model)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    // Require the user to have a confirmed email before they can log on.
                    ApplicationUser user = await _userManager.FindByEmailAsync(model.Email);

                    if (user != null)
                    {
                        if (!await _userManager.IsEmailConfirmedAsync(user))
                            throw new Exception("You must have a confirmed email to log in.");
                    }

                    SignInResult result = await _signInManager.PasswordSignInAsync(model.Email, model.PasswordHash, isPersistent: true, lockoutOnFailure: true);

                    if (result.IsLockedOut)
                        throw new Exception("IsLockedOut");

                    if (result.Succeeded)
                    {
                        return Ok(user);
                        //return BuildToken(userInfo);
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Invalid login attempt");
                        return BadRequest(ModelState);
                    }
                }
                else
                    return BadRequest(ModelState);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("AccessDenied")]
        public ActionResult AccessDenied()
        {
            return Ok("AccessDenied");
        }

    }
}
