﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.Seguridad
{
    
    public partial class ApplicationRol: IdentityRole
    {
        public ApplicationRol()
        {
            ApplicationRoleClaim = new HashSet<ApplicationRoleClaim>();
        }
        [NotMapped]
        public ICollection<ApplicationRoleClaim> ApplicationRoleClaim { get; set; }
    }
}
