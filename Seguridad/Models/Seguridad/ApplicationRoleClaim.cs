﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.Seguridad
{
    [NotMapped]
    public partial class ApplicationRoleClaim : IdentityRoleClaim<string>
    {
        public ApplicationRoleClaim()
        {
            ApplicationUserClaim = new HashSet<ApplicationUserClaim>();
        }
        public short IdSistema { get; set; }
        public DateTime FechaRoleClaim { get; set; }

        public virtual Sistema Sistema { get; set; }
        public ICollection<ApplicationUserClaim> ApplicationUserClaim { get; set; }
    }
}
