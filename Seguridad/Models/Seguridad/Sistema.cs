﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Seguridad
{
    [Table("Sistema", Schema = "Seguridad")]
    public partial class Sistema
    {
        public Sistema()
        {
            ApplicationRoleClaim = new HashSet<ApplicationRoleClaim>();
        }

        [Key]
        public short IdSistema { get; set; }
        [Required]
        [StringLength(50)]
        public string NombreSistema { get; set; }
        [Required]
        [StringLength(50)]
        public string Url { get; set; }
        [Required]
        [StringLength(50)]
        public string Icono { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaSistema { get; set; }
        public ICollection<ApplicationRoleClaim> ApplicationRoleClaim { get; set; }
    }
}
