﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Seguridad
{
    public class UserInfo
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public int IdEmpleado { get; set; }
    }
}
