﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.Seguridad
{
    [NotMapped]
    public partial class ApplicationUserClaim: IdentityUserClaim<string>
    {
        public short IdClaim { get; set; }
        public int IdApplicationRoleClaim { get; set; }
        public DateTime FechaUserClaim { get; set; }

        public virtual Claim Claim { get; set; }
        public virtual ApplicationRoleClaim ApplicationRoleClaim { get; set; }
    }
}
