﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Seguridad
{
    [Table("Claim", Schema = "Seguridad")]
    public partial class Claim
    {
        public Claim()
        {
            ApplicationUserClaim = new HashSet<ApplicationUserClaim>();
        }
        public short IdClaim { get; set; }
        [StringLength(50)]
        public string NombreClaim { get; set; }
        public bool? Activo { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaClaim { get; set; }
        public ICollection<ApplicationUserClaim> ApplicationUserClaim { get; set; }
    }
}
