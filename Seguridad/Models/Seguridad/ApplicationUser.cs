﻿using Microsoft.AspNetCore.Identity;
using Models.Employee;
//using Models.Employee;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.Seguridad
{
    [NotMapped]
    public partial class ApplicationUser : IdentityUser
    {
        public int IdEmpleado { get; set; }
        public virtual Empleado Empleado { get; set; }

        public DateTime FechaUsuario { get; set; }


    }
}