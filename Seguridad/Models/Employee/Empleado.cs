﻿using Models.Seguridad;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.Employee
{
    [Table("Empleado", Schema = "Empleado")]
    public partial class Empleado
    {
        public Empleado()
        {
        }

        public int IdEmpleado { get; set; }
        public int IdPersona { get; set; }
        public short IdUnidad { get; set; }
        public bool Activo { get; set; }
        public short IdTipoEmpleado { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaEmpleado { get; set; }
        public virtual ApplicationUser Usuario { get; set; }
    }
}
