﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Models.Employee;
using Models.Seguridad;
using System;

namespace Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }
        public virtual DbSet<Claim> Claim { get; set; }
        public virtual DbSet<Empleado> Empleado { get; set; }
        public virtual DbSet<Sistema> Sistema { get; set; }
        public virtual DbSet<ApplicationRol> ApplicationRol { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            #region Claim
            modelBuilder
                .Entity<Claim>()
                .HasKey(e => e.IdClaim)
                .HasName("PK_CLAIM")
                .ForSqlServerIsClustered(false);
            #endregion

            #region Empleado
            modelBuilder
                .Entity<Empleado>()
                .HasKey(e => e.IdEmpleado)
                .HasName("PK_EMPLEADO")
                .ForSqlServerIsClustered(false);

            modelBuilder
                .Entity<Empleado>()
                .HasOne(e => e.Usuario)
                .WithOne(b => b.Empleado)
                .HasForeignKey<ApplicationUser>(b => b.IdEmpleado);
            #endregion

            #region ApplicationUser
            modelBuilder
                .Entity<ApplicationUser>()
                .ToTable("AspNetUsers", "Seguridad");
            #endregion

            #region IdentityRole
            modelBuilder
                .Entity<IdentityRole>()
                .ToTable("AspNetRoles", "Seguridad");
            #endregion

            #region ApplicationRoleClaim
            modelBuilder
                .Entity<IdentityRoleClaim<string>>()
                .ToTable("AspNetRoleClaims", "Seguridad");

            modelBuilder
                .Entity<ApplicationRoleClaim>()
                .HasOne(e => e.Sistema)
                .WithMany(b => b.ApplicationRoleClaim)
                .HasForeignKey(e => e.IdSistema);
            #endregion

            #region ApplicationUserClaim
            modelBuilder
                .Entity<IdentityUserClaim<string>>()
                .ToTable("AspNetUserClaims", "Seguridad");

            modelBuilder
                .Entity<ApplicationUserClaim>()
                .HasOne(e => e.Claim)
                .WithMany(b => b.ApplicationUserClaim)
                .HasForeignKey(e => e.IdClaim);

            modelBuilder
                .Entity<ApplicationUserClaim>()
                .HasOne(e => e.ApplicationRoleClaim)
                .WithMany(b => b.ApplicationUserClaim)
                .HasForeignKey(e => e.IdApplicationRoleClaim);
            #endregion

            #region 
            #endregion
            #region 
            #endregion
            #region 
            #endregion
            #region 
            #endregion
            #region 
            #endregion
            #region 
            #endregion
            #region 
            #endregion
            #region 
            #endregion
        }
    }
}
