﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IdentityDemo.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace IdentityDemo.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _rolManager;
        public HomeController(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _rolManager = roleManager;
        }
        public async Task<IActionResult> Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                                var user = await _userManager.GetUserAsync(HttpContext.User);
               // await _userManager.AddClaimAsync(user, new Claim("CategoriaEmpleado", "4"));
                var claims = User.Claims.ToList();
            }
                return View();
        }

        public IActionResult Privacy()
        {
            /*
            if (User.Identity.IsAuthenticated)
            {
                await _rolManager.CreateAsync(new IdentityRole("Admin"));
                var user = await _userManager.GetUserAsync(HttpContext.User);
                await _userManager.AddToRoleAsync(user, "Admin");
            }
            */
            return View();
        }

      //  [Authorize(Roles = "Admin")]
      [Authorize(Policy = "PolicyCategoriaEmpleado")]
        public IActionResult Contact()
        {
            ViewData["Title"] = "Your contact page.";
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
