﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Route("api/Pais/{PaisId}/[controller]")]
    [ApiController]
    public class ProvinciasController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public ProvinciasController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Provincia> GetAll(int PaisId)
        {
            return _context.Provincias.Where(x => x.PaisId == PaisId).ToList();
        }

        [HttpGet("{id}", Name = "provinciaById")]
        public IActionResult GetById(int id)
        {
            var pais = _context.Provincias.FirstOrDefault(x => x.Id == id);

            if (pais == null)
                return NotFound();

            return new ObjectResult(pais); // = return OK(pais);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Provincia provincia, int PaisId)
        {
            provincia.PaisId = PaisId;

            if (ModelState.IsValid)
            {
                _context.Provincias.Add(provincia);
                _context.SaveChanges();
                return new CreatedAtRouteResult("provinciaById", new { id = provincia.Id }, provincia);
            }

            return BadRequest(ModelState);
        }

        [HttpPut("{id}")]
        public IActionResult Update([FromBody] Provincia provincia, int id)
        {
            if (provincia.Id != id)
                return BadRequest();

            _context.Entry(provincia).State = EntityState.Modified;
            _context.SaveChanges();
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var provincia = _context.Provincias.FirstOrDefault(x => x.Id == id);

            if (provincia == null)
                return NotFound();

            _context.Provincias.Remove(provincia);
            _context.SaveChanges();
            return Ok(provincia);
        }
    }
}